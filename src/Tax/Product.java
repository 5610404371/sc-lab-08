package Tax;

public class Product implements Taxable{
	String name;
	double price;
	public Product(String name, double price) {
		this.name = name;
		this.price = price;
	}
	
	@Override
	public double getTax() {
		double tax;
		tax = (price*7)/100;
		return tax;
	}

}
