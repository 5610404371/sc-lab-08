package Tax;

public class Person implements Taxable{
	String name;
	double income;
	public Person(String name , double income) {
		this.name = name;
		this.income = income; 

		
	}
	@Override
	public double getTax() {
		double tax = 0;
		if (income <= 300000){
			tax = (income*5)/100;
		}else if(income > 300000){
			tax = income - 300000; 
			tax = (income*10)/100 + 15000;
		}	
		
		return tax;
	}

}
