package Tax;

public class Company implements Taxable{
	String name;
	double income, expense;
	
	public Company(String name, double income,double expense) {
		this.name = name;
		this.income = income;
		this.expense = expense;
		
	}
	
	@Override
	public double getTax() {
		double tax;
		tax = ((income - expense)*30)/100;
		return tax;
	}

}
