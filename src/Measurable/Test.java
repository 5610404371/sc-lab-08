package Measurable;

public class Test {

	public static void main(String[] args) {
		Measurable[] person = new Measurable[5];
		person[0] = new Person("Mr.Daniel", 180);
		person[1] = new Person("Mr.Frank", 174);
		person[2] = new Person("Ms.Jasmin", 168);
		person[3] = new Person("Ms.Cara", 172);
		person[4] = new Person("Ms.Daisy", 163);
		
		Data data = new Data();
		System.out.println("Average height person is "+data.average(person));
		System.out.println("Minimum height of person is "+data.min(person));
		
		Measurable[] country = new Measurable[3];
		country[0] = new Country("Thailand", 280000);
		country[1] = new Country("Singapore", 55000);
		country[2] = new Country("Japan", 158000);
		System.out.println("Minimum area of country is "+data.min(country));
		
		Measurable[] bankaccount = new Measurable[3];
		bankaccount[0] = new BankAccount(20000);
		bankaccount[1] = new BankAccount(500);
		bankaccount[2] = new BankAccount(42000);
		System.out.println("Minimum balance of bankaccount is "+data.min(bankaccount));
		
		
	}

}
