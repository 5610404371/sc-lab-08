package Measurable;

public interface Measurable {
	public double getMeasure(); 
}
