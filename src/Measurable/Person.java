package Measurable;

public class Person implements Measurable{
	String name;
	int height;
	public Person(String name, int height) {
		this.name = name;
		this.height = height;
	}
	@Override
	public double getMeasure() {
		return height;
	}
	


}
